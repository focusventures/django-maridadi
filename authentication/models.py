from django.db import models
from django.db.models import Sum
from django.contrib.auth.models import AbstractUser, UserManager
from django.utils.translation import ugettext_lazy as _
from django.core.validators import RegexValidator
from django.contrib.auth.models import  AbstractBaseUser, PermissionsMixin
from stdimage import StdImageField
# from apps.activity.models import Betslip


from django.utils import timezone
from django.utils.http import urlquote


from django.contrib.auth.models import BaseUserManager

class CustomUserManager(BaseUserManager):
    def _create_user(self, phone_number, password,
                     is_staff, is_superuser, **extra_fields):
        """
        Creates and saves a User with the given email and password.
        """
        now = timezone.now()
        if not phone_number:
            raise ValueError('The given phone number must be set')
        user = self.model(phone_number=phone_number,
                          is_staff=is_staff, is_active=True,
                          is_superuser=is_superuser, last_login=now,
                          date_joined=now, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, phone_number, password=None, **extra_fields):
        return self._create_user(phone_number, password, False, False,
                                 **extra_fields)

    def create_superuser(self, phone_number, password, **extra_fields):
        return self._create_user(phone_number, password, True, True,
                                 **extra_fields)


class CustomUser(AbstractBaseUser, PermissionsMixin):
    """
    A fully featured User model with admin-compliant permissions that uses
    a full-length email field as the username.

    Email and password are required. Other fields are optional.
    """
    phone_regex = RegexValidator(regex=r'^\d{10}$',
                                 message="Phone number must be entered in the format: '0721XXXXXX'. Up to 10 digits allowed.")
    Male = 'M'
    Female = 'F'
    genders = (
        ('M', 'Male'),
        ('F', 'Female'),
    )
    alias = models.CharField(max_length=12, help_text='username is required', null=True)
    email = models.EmailField(blank=False, default="pymontuser@gmail.com")
    username = models.CharField(max_length=12, help_text='username is required', null=True)
    county = models.CharField(max_length=50, null=True, help_text='Enter your town e.g ruiru or kericho')
    phone_number = models.CharField(validators=[phone_regex], blank=True, max_length=15, unique=True)
    first_name = models.CharField(_('first name'), max_length=30, blank=True, null=True)
    middle_name = models.CharField(_('Middle name'), max_length=30, blank=True, null=True)
    last_name = models.CharField(_('last name'), max_length=30, blank=True, null=True)
    gender = models.CharField(choices=genders, max_length=12, default='F', null=True)
    avatar = StdImageField(_("Profile Picture"), upload_to='media/',
                          variations={'thumbnail': {'width': 100, 'height': 75}}, null=True, blank=True, default='user.jpg')
    is_staff = models.BooleanField(_('staff status'), default=False,
        help_text=_('Designates whether the user can log into this admin '
                    'site.'))
    # account_type = models.CharField(_('Account Type'), max)
    is_active = models.BooleanField(_('active'), default=True,
        help_text=_('Designates whether this user should be treated as '
                    'active. Unselect this instead of deleting accounts.'))
    date_joined = models.DateTimeField(_('date joined'), default=timezone.now)

    objects = CustomUserManager()

    USERNAME_FIELD = 'phone_number'
    REQUIRED_FIELDS = []

    class Meta:
        verbose_name = _('user')
        verbose_name_plural = _('users')

    def get_absolute_url(self):
        return "/users/%s/" % urlquote(self.email)

    def get_full_name(self):
        """
        Returns the first_name plus the last_name, with a space in between.
        """
        full_name = '%s %s' % (self.first_name, self.last_name)
        return full_name.strip()

    def get_short_name(self):
        "Returns the short name for the user."
        return self.first_name

    def get_phone_number(self):
        return self.user.phonenumber

          


