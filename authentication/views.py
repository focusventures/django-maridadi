from django.shortcuts import render
from django.contrib.auth import authenticate,logout, login as auth_login
from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect
from django.contrib import messages


from authentication.models import CustomUser

from .forms import SignUpForm, ProfileForm,ChangePasswordForm


# Create your views here.



def signup(request):
    if request.user.is_authenticated:
        return redirect('/')

    context = {}
    """
    :param request:
    :return: admission form to
    logged in user.
    """
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            phone = request.POST.get('phone_number')
            username = request.POST.get('username')
            password = request.POST.get('passowrd')
            user = CustomUser.objects.create_user(
                phone_number=phone,
                username = username,
                password = password
            ) 
            user.save()
        
            return redirect('/login')
    else:
        form = SignUpForm()
    context = {'form': form}
    return render(request, 'authentication/signup.html', context)



def login(request):
    if request.user.is_authenticated:
        return redirect('/')
    context = {}

    if request.method == "POST":
        phone = request.POST.get('phone_number')
        password = request.POST.get('password')
        

        auth = authenticate(phone_number=phone, password=password)

        if auth is not None:
            auth_login(request, auth)
            return redirect('/')

    return render(request, 'authentication/login.html', context)



def reset(request):
    if request.user.is_authenticated:
        return redirect('/')
    context = {}
    return render(request, 'authentication/reset.html', context)



def confirm(request):
    if request.user.is_authenticated:
        return redirect('/')
    context = {}
    return render(request, 'authentication/confirm.html', context)




# @login_required
def profile(request):
    context = {}
    print(request.user)
    user = CustomUser.objects.get(phone_number=request.user)
    """
    :param request:
    :return: admission form to
    logged in user.
    """
    if request.method == 'POST':
        form = ProfileForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
          
            return redirect('/profile')
    else:
        form = ProfileForm(instance=user)
        password_form = ChangePasswordForm()
    context = {'form': form, "password_form": password_form}
    return render(request, 'authentication/profile.html', context)


def change_password(request):
    context = {}
    """
    :param request:
    :return: admission form to
    logged in user.
    """
    if request.method == 'POST':
        form = ChangePasswordForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
          
    return redirect('/profile')


def user_logout(request):
    logout(request)
    return redirect('/login')