from django import forms

from .models import Shop, Address, Post



class ShopForm(forms.ModelForm):
    class Meta:
        model = Shop
        fields = '__all__'



class AddressForm(forms.ModelForm):
    class Meta:
        model = Address
        fields = '__all__'


class PostForm(forms.ModelForm):
    class Meta:
        model = Post
        fields = '__all__'