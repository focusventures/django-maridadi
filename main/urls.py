from django.urls import path

from . import views


urlpatterns = [
    path('', views.index, name="index"),
    path('about', views.about, name="about_us"),
    path('services', views.services, name="our_services"),
    path('products', views.products, name="our_products"),
    path('training', views.training, name="training"),
    path('invest', views.invest, name="invest"),
    path('contact', views.contact, name="contact"),
 
]