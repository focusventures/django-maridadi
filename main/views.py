from django.shortcuts import render, HttpResponse
from django.http import JsonResponse




def index(request):

    return render(request, 'main/index.html')

def about(request):

    return render(request, 'main/about.html')

def services(request):
 
    return render(request, 'main/services.html')

def training(request):
   
    return render(request, 'main/training.html')



def contact(request):
  
    return render(request, 'main/contact.html')


def products(request):
    return render(request, 'main/products.html')


def invest(request):

    return render(request, 'main/invest.html')